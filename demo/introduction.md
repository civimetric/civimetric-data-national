Tous les automnes, le Gouvernement présente le projet de budget de la France pour l’année suivante.
L'objectif est d'équilibrer les dépenses et les recettes de l'Etat français.

Nous vous proposons d'**imaginer que vous êtes ministre de l'Action et des Comptes publics**.
A votre avis, quelle serait la meilleure combinaison de dépenses et de recettes qui répondrait aux attentes de l'ensemble de la population française ?
Allez-vous réussir à satisfaire les français ?

Ce questionnaire devrait vous prendre **15 minutes** au total.
Bonne séance !