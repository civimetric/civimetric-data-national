# Agriculture, alimentation, forêt et affaires rurales

**Définition** : La mission "Agriculture, alimentation, forêt et affaires rurales" corresponds aux dépenses liées à la compétitivité agricole, au développement des forêts, et à la qualité alimentaire.

**En 2018**, le budget prévisionnel estimé est de _3,43 milliards_ d'euros.

![Agriculture](agriculture.jpg "Pommiers")