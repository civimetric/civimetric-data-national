# Action Extérieure de l'Etat

**Définition** : La mission "Action Extérieure" corresponds à toutes les dépenses liées au rayonnement et l’influence de la diplomatie culturelle française à l’étranger.

**En 2018**, le budget prévisionnel total est estimé à _3 milliards_ d'euros.

![Etranger](action-exterieure.jpg)